﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jyrocam : MonoBehaviour
{
    public bool isGyroActive = false;
    public GameObject parent;

    private float _angleMultiplier = 1.75f;
    // Start is called before the first frame update
    void Start()
    {
        parent.transform.position = transform.position;
        transform.parent = parent.transform;
        Input.gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        var gyro = Input.gyro.rotationRateUnbiased * _angleMultiplier;
        
        if (Time.time > 3)
            if (isGyroActive)
            {
                parent.transform.Rotate(0, -gyro.y, 0);
                this.transform.Rotate(-gyro.x, 0, 0);
            }
    }
}
