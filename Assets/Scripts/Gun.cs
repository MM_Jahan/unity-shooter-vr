﻿using System;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;
using Leap.Unity.Infix;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject gunEnd;
    public float bulletForce = 3000;
    public float yOffset;
    public ParticleSystem muzzleFlash;
    [Header("Ammo")]
    public int magazines;
    public int bullets;
    public float bulletReloadDelay;
    public float magazineReloadDelay;
    public float bulletDamage = 25;
    [Header("Audio")] 
    public AudioClip bulletReload;
    public AudioClip fire;
    public AudioClip mazReload;
    public AudioClip empty;
    public AudioClip gunDrawed, gunUndrawed;
    public AudioClip gripped, ungripped;
    
    private GameObject[] _gunComponents;
    private int _initialBullet, _initialMagazines;
    [HideInInspector]
    public bool isMazArmed = true;
    private float _fireTimeCounter;
    private AudioSource _audioSource;
    private bool _gunInHand;
    private bool _gunDrawed = true;
    private bool _secondaryGriped;
    private MeshRenderer[] _meshRenderers;
    // Start sets reference
    void Start()
    {
        _initialBullet = bullets;
        _initialMagazines = magazines;
        _audioSource = GetComponent<AudioSource>();
        _gunComponents = GameObject.FindGameObjectsWithTag("Gun components");
        _meshRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (var VARIABLE in _gunComponents)
        {
            //print(VARIABLE);
        }
    }

    //Instantiates projectile on leap command
    public void Fire()
    {
        if (!_gunDrawed) return; //check if gun is active
        if (!_gunInHand) return; //player has gun in hand
        if( bulletReloadDelay <=_fireTimeCounter) // delay for each shot
            if (bullets > 0 && isMazArmed)
            {
                --bullets;
                _fireTimeCounter = 0;
                //checks bullets after reduction
                if (bullets == 0) isMazArmed = false;
            
                //rotation + bullet required rotation
                Quaternion bulletRotation = Quaternion.Euler(gameObject.transform.rotation.ToAngleAxisVector().x,
                    gameObject.transform.rotation.ToAngleAxisVector().y,
                    gameObject.transform.rotation.ToAngleAxisVector().z);
                
                //instantiation
                GameObject bullet = Instantiate(bulletPrefab, gunEnd.transform.position, 
                    bulletRotation);
                bullet.GetComponent<Rigidbody>().AddRelativeForce(0,0, bulletForce);
                bullet.GetComponent<Projectile>().damage = bulletDamage;
                Destroy(bullet, 10);
                
                //plays muzzle flash
                muzzleFlash.Play();

                //Fire animation
                _gunComponents[0].GetComponent<Animation>().Play("Empty|Fire"); //GetClip("Empty|Fire")
                _gunComponents[1].GetComponent<Animation>().Play();
                _gunComponents[2].GetComponent<Animation>().Play();
                
                //Sound
                _audioSource.clip = fire;
                _audioSource.volume = 0.35f;
                _audioSource.Play();
                
            }
            else
            {
                isMazArmed = false; // disarms the gun
                //empty sound
                _audioSource.clip = empty;
                _audioSource.volume = .5f;
                _audioSource.Play();
            }
    }

    public void ReloadMaz()
    {
        if (_secondaryGriped) return;
        if (magazines > 0 && bullets <= 0)
        {
            bullets = _initialBullet; // reset bullets
            magazines--;
            
            //Maz reload animations
            _gunComponents[0].GetComponent<Animation>().Play("Empty|MazSwitch");
            _gunComponents[3].GetComponent<Animation>().Play();
            
            //Sound
            _audioSource.clip = mazReload;
            _audioSource.volume = 3;
            _audioSource.Play();
        }
    }

    public void ReloadBullet()
    {
        if (bullets > 0 && !isMazArmed)
        {
            isMazArmed = true; // arms the gun
            
            //bullet reload animations
            _gunComponents[0].GetComponent<Animation>().Play("Empty|Hit");
            _gunComponents[4].GetComponent<Animation>().Play();
            
            //
            _audioSource.clip = bulletReload;
            _audioSource.volume = 10;
            _audioSource.Play();
        }
    }
    
    void FixedUpdate()
    {
        _fireTimeCounter += Time.deltaTime;
        if(_gunDrawed) //check if gun is drawed
            SetGunTransform();
    }

    public RaycastHit Raycast()
    {
        RaycastHit hit;
        
        //draw debug raycast
        Debug.DrawRay(gunEnd.transform.position, gunEnd.transform.TransformDirection(Vector3.back) * 1000, Color.green);
        
        Physics.Raycast(gunEnd.transform.position, gunEnd.transform.TransformDirection(Vector3.back) * 1000,out hit,1000000 ,1 << 8);
        return hit;
    }

    private void SetGunTransform()
    {
        if (Hands.Right != null)
        {
            //set gun POSITION to the left hand
            transform.position = Hands.Right.PalmPosition.ToVector3();
            
            //make gun visible
            foreach (var renderer in _meshRenderers) renderer.enabled = true;
            
            _gunInHand = true;

            if (Hands.Left != null)
            {
                if (!_secondaryGriped) return; // check if secondary grip is griped.
                
                //set gun DIRECTION to right hand();
                transform.LookAt(new Vector3(Hands.Left.PalmPosition.ToVector3().x,
                    Hands.Left.PalmPosition.ToVector3().y + yOffset,
                    Hands.Left.PalmPosition.ToVector3().z));
            }
        }
        else
        {
            _gunInHand = false;
            //make gun invisible
            foreach (var renderer in _meshRenderers) renderer.enabled = false;
        }
    }
    
    //called on hand draw/undraw gesture.
    public void GunDrawUndraw()
    {
        _gunDrawed = !_gunDrawed;
        
        if(_gunDrawed) _audioSource.clip = gunDrawed;
        else 
        {
            _audioSource.clip = gunUndrawed;
            _gunInHand = false;
        }
        
        _audioSource.Play();
        //make gun visible/invisible
        foreach (var renderer in _meshRenderers)
            renderer.enabled = !renderer.enabled;
        
    }

    //called when right hand is closed/opened.
    public void SecondaryGriped(bool griped)
    {
        _secondaryGriped = griped;
        
        if (!_gunInHand) return; //dont run if gun not drawed.

        //rotate gun when ungriped
        if (!griped)
        {
            transform.rotation =
                Quaternion.Euler(-25, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
            _audioSource.clip = ungripped;
        }else
            _audioSource.clip = gripped;
        
        _audioSource.Play();
    }
}
