﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float speed;
    public Vector3 direction;
    public float timeTillTurn;
    
    private Rigidbody _rigidbody;
    private float _timePassed;
    private GameManager _gameManager;
    //sets reference and start force
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.AddForce(direction * speed / 2);
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    //change direction on duration
   void Update()
    {
        if (timeTillTurn <= _timePassed)
        {
            direction = -direction;
            _rigidbody.AddForce(direction * speed);
            _timePassed = 0;
        }
        _timePassed += Time.deltaTime;
    }

   //Destroy this and bullet, add kill to game manager
   private void OnCollisionEnter(Collision other)
   {
       if (other.gameObject.CompareTag("Projectile"))
       {
           GetComponent<AudioSource>().Play();
           _gameManager.points++;
           GetComponent<Rigidbody>().Sleep();
           GetComponent<Target>().enabled = false;
       }
   }
}
