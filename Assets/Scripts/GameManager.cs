﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("UI reference")]
    public Text[] ammoText;
    public Text[] pointText;
    public GameObject cursor;
    public Vector2 cursorScale;
    public Gun gun;
    public int points;

    private int _points;
    private float _cursorResizeDelay = 0.1f;
    private int _bulletValueChange;
    private float _timeCounter;
    private float _cursorScaleMultiplier = 1;
    private SpriteRenderer _cursorColor;
    private GameObject _camera;
    // Start is called before the first frame update
    void Start()
    {
        _bulletValueChange = gun.bullets;
        _cursorColor = cursor.GetComponent<SpriteRenderer>();
        _camera = Camera.main.gameObject;
    }

    void Update()
    {
        CursorScale();
        UIUpdate();
    }

    //Updates the UI values
    void UIUpdate()
    {
        pointText[0].text = "x " + points;
        pointText[1].text = "x " + points;
        
        
        ammoText[0].text = AddZero(gun.magazines) + "/" + AddZero(gun.bullets);
        ammoText[1].text = AddZero(gun.magazines) + "/" + AddZero(gun.bullets);

        //Updates cursor properties
        CursorUpdate();
    }

    private void CursorUpdate()
    {
        //change COLOR when out of ammo
        if (gun.isMazArmed)
            _cursorColor.color = Color.white;
        else if (gun.bullets == 0 && !gun.isMazArmed)
            _cursorColor.color = Color.black;
        else if (gun.bullets == 10 && !gun.isMazArmed)
            _cursorColor.color = Color.gray;

        //set the POSITION of the cursor
        cursor.transform.position = gun.Raycast().point;

        //set cursor SCALE over distance
        Vector2 cursorLiveScale = cursorScale * gun.Raycast().distance;
        cursor.transform.localScale = cursorLiveScale * _cursorScaleMultiplier;

        //set the DIRECTION of the cursor to the camera
        Vector3 cameraPosition = _camera.transform.position;
        cursor.transform.LookAt(new Vector3(cameraPosition.x, transform.position.y,
            cameraPosition.z));

        if (OnValueChange(gun.bullets) && gun.bullets != 10)
            _timeCounter = 0; // this sets the CursorScale() active
    }

    private bool OnValueChange(int value)
    {
        if (_bulletValueChange == value)
            return false;
        
        _bulletValueChange = value;
        return true;
        
    }

    private void CursorScale()
    {
        _timeCounter += Time.deltaTime;
        
        //sets the scaleMultiplier to default
        if (_timeCounter > _cursorResizeDelay)
            _cursorScaleMultiplier = 1;
        
        //lerps in scale
        else if (_timeCounter < _cursorResizeDelay / 2)
            _cursorScaleMultiplier = Mathf.LerpUnclamped(1, 2,
                _timeCounter * (1 / (_cursorResizeDelay / 2)));

        //lerps in reverse scale
        else if (_timeCounter > _cursorResizeDelay / 2)
            _cursorScaleMultiplier = Mathf.LerpUnclamped(2,1,
               (_timeCounter - _cursorResizeDelay / 2) * (1 / (_cursorResizeDelay / 2)));
        
    }

    private string AddZero(int number)
    {
        if (number < 10)
            return "0" + number;
        return number.ToString();
    }

}
