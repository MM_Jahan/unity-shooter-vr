﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float damage;
    private void OnCollisionEnter(Collision other)
    {
        //gets the dust particle system
        ParticleSystem dust = GetComponentInChildren<ParticleSystem>();
        
        //disables objects components
        gameObject.GetComponent<Rigidbody>().Sleep();
        gameObject.GetComponent<BoxCollider>().enabled = false;
        
        //turn red on hit with enemy
        if (other.gameObject.CompareTag("Enemy"))
        {
            //main module of the child particle system of dust
            ParticleSystem.MainModule main =
                dust.gameObject.GetComponentsInChildren<ParticleSystem>()[1].main;
            print("Enemy");
            
            main.startColor = Color.red;
        }
        
        //play particle and destroy after 2 seconds
        dust.Play();
        Destroy(gameObject, 2);
    }
}
