﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float playerSpeed = 10;
    private Quaternion _startRotation;
    private Rigidbody _rigidbody;
    private bool _isMoving;
    private AudioSource _audioSource;
    // Gets reference
    void Start() {
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
        _startRotation= transform.rotation;
        //todo fix rig rotation
    }

    
    //active/deactivates walking on leap command
    public void MoveForwardActive()
    {
        _isMoving = true;
        _audioSource.Play();
    }

    public void MoveForwardDeactive()
    {
        _isMoving = false;
        _audioSource.Stop();
    }
    
    //Walks
    void Update()
    {
        if(_isMoving)
            _rigidbody.AddRelativeForce(Vector3.forward * (Time.deltaTime * playerSpeed));
    }

    public void ResetGyro()
    {
        transform.rotation = _startRotation; //rotation of this
        GetComponentsInChildren<Transform>()[1].rotation = _startRotation; //rotation of child
    }
    
}
