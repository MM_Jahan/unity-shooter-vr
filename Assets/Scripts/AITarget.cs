﻿using System;
using System.Collections;
using UnityEngine;

public class AITarget : MonoBehaviour
{
    public float speed;
    public float health = 100;
    public float waitTime = 2;
    public Transform[] targets;
    private GameManager _gameManager;

    [Header("audio")] 
    public AudioClip death;
    public AudioClip hit;
    public AudioClip walking;
    
    //For rotation
    private float _timeCounter;          //time for rotation
    private bool _rotate;                //allow rotation
    private bool _rotateReverse;         //direction
    private int _targetIndex;            //waypoint counter
    
    private Animator _animator;
    private bool _dead;
    private Quaternion _bullet;
    private Quaternion _lastDirection;
    private bool _waypoint = true;
    private float _initialHealth;
    private int _headShotMultiplier = 2;

    private AudioSource _audioSource;
    //sets reference and start force
    void Start()
    {
        _initialHealth = health;
        _animator = GetComponentInChildren<Animator>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _audioSource = GetComponent<AudioSource>();
        _targetIndex = 0;
        if (targets[1] == null) // check if waypoints was been set
            _waypoint = false;
    }

    void FixedUpdate()
    {
        if (_dead) // if dead dont run after if
        {
            _timeCounter += Time.deltaTime;
            
            //rotate acordingly from the direction of the bullet
            transform.rotation = Quaternion.Lerp(transform.rotation,
                Quaternion.Euler(_bullet.eulerAngles.x,
                _bullet.eulerAngles.y + 90,
                _bullet.eulerAngles.z+90),_timeCounter * 0.3f);
            
            //exit FixedUpdate
            return;
        }

        if (_waypoint) 
            MoveToNextWaypoints();
        else
            _animator.SetBool("walk", false); //set idle animation

    }

    //move to the next waypoint
    private void MoveToNextWaypoints()
    {
        //gets active waypoint and removes y coordinates.
        Vector3 activeWaypoint = new Vector3(targets[_targetIndex].position.x, transform.position.y, targets[_targetIndex].position.z);

        //if AI is not close to the waypoint: go close to the waypoint
        if (Vector3.Distance(transform.position, activeWaypoint) > .1f)
        {            
            _animator.SetBool("walk", true); //set walking animation
            SetAudio(walking,true);
            float step = speed * Time.deltaTime; //amount of movement in time

            //moves the AI to active
            transform.position = Vector3.MoveTowards(transform.position, activeWaypoint, step);

            _lastDirection = transform.rotation;
        }

        else //when reached waypoint
        {
            _timeCounter += Time.deltaTime;

            _audioSource.Stop();
            _animator.SetBool("walk", false); //set idle animation

            //ROTATE towards next waypoint
            Vector3 lookRawDirection = new Vector3(targets[FindNextIndex(_targetIndex, targets)].position.x,
                transform.position.y,
                targets[FindNextIndex(_targetIndex, targets)].position.z);
            
            transform.LookAt(lookRawDirection);
            var lookDirection = Quaternion.Euler(transform.rotation.eulerAngles.x,
                transform.rotation.eulerAngles.y-90,transform.rotation.eulerAngles.z);
            
            transform.rotation = Quaternion.LerpUnclamped(_lastDirection,lookDirection,_timeCounter * (1 / waitTime));
            
            //reset timer and process next movement variables
            if (_timeCounter < waitTime) return;
            _timeCounter = 0;
            
            _targetIndex = FindNextIndex(_targetIndex, targets);
        }
    }

    //find the next index of the array
    private int FindNextIndex(int indexer, Array array)
    {
        if (array.Length > ++indexer) 
            return indexer;
        return 0;
    }

    //Destroy this and bullet, add kill to game manager
     private void OnCollisionEnter(Collision other)
     {
         if (_dead) return;
         if (!other.gameObject.CompareTag("Projectile")) return;
            
         //get raw damage and give processed damge
         var damage = other.gameObject.GetComponent<Projectile>().damage;
         if (other.transform.position.y >= transform.position.y + 1.8f) //headshot
             damage *= _headShotMultiplier;
         if (damage > health) damage = health;
         
         //take damage
         health -= damage;
         _gameManager.points += (int)damage;
         
         //die
         if (health <= 0)
         {
             SetAudio(death);
             _gameManager.points += (int)_initialHealth;
             _animator.SetTrigger("dead");
             _dead = true;

             StartCoroutine(DieTimer());
             //set a reference of rotation form the bullet
             _bullet = other.transform.rotation;
             //reset time counter for lerping fall direction in FixedUpdate
             _timeCounter = 0;
         }
         else
         {
             SetAudio(hit);
         }
     }

     IEnumerator DieTimer()
     {
         yield return new WaitForSeconds(1);
         Destroy(GetComponent<Rigidbody>());
         GetComponent<BoxCollider>().enabled = false;
     }
     /// <summary>
     /// sets audioClip and takes sets loop if needed
     /// </summary>
     /// <param name="audioClip">Enter audio clip</param>
     /// <param name="loop">wont loop on default</param>
     /// <param name="volume">default = 1</param>
     private void SetAudio(AudioClip audioClip, bool loop = false, float volume = 3)
     {
         //stability check for walking sound
         if (_audioSource.clip != walking && _audioSource.isPlaying) return;
         
         //change volume looping, play state, audio clip if not the same
         if (_audioSource.clip != audioClip) _audioSource.clip = audioClip;
         if(!_audioSource.isPlaying) _audioSource.Play();
         if (_audioSource.loop != loop) _audioSource.loop = loop;
         
         _audioSource.volume = volume;
     }
}

